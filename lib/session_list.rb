class SessionList
  REDIS_KEY = 'current_sessions'

  def self.redis
    @redis ||= ::Redis.new(url: 'redis://localhost:6379/1')
  end

  def self.all
    redis.smembers(REDIS_KEY)
  end

  def self.clear_all
    redis.del(REDIS_KEY)
  end

  def self.count
    redis.scard(REDIS_KEY)
  end

  def self.add(uid)
    redis.sadd(REDIS_KEY, uid)
  end

  def self.include?(uid)
    redis.sismember(REDIS_KEY, uid)
  end

  def self.remove(uid)
    redis.srem(REDIS_KEY, uid)
  end
end
