class EventSearch
  attr_accessor :cities, :topic_ids, :from_date, :to_date

  class << self
    def load_saved(params)
      parse_date = -> string do
        return nil unless string.present?

        begin
          Date.parse(string)
        rescue ArgumentError
          nil
        end
      end

      new.tap do |es|
        es.topic_ids = params['topic_ids']
        es.cities = params['cities']
        es.from_date = parse_date.(params['from_date'])
        es.to_date = parse_date.(params['to_date'])
      end
    end
  end

  def initialize
    @topic_ids, @cities = [], []
    @from_date, @to_date = nil, nil
  end

  def events
    search
  end

  def hits
    search.count
  end

  def as_json(options = {})
    {
      cities: cities.sort,
      topic_ids: topic_ids.sort,
      from_date: from_date,
      to_date: to_date
    }
  end

  private
  def search
    scope = Event.all.order(updated_at: :desc)

    unless cities.blank?
      scope = scope.by_city(cities)
    end

    unless topic_ids.blank?
      scope = scope.by_topic_id(topic_ids)
    end

    if from_date && to_date
      scope = scope.by_start_date(from_date, to_date)
    end

    scope
  end
end
