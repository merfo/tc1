require 'rails_helper'

RSpec.describe 'Search', type: :feature, js: true do
  let(:city_filter_selector) { '#event-city-filter' }
  let(:city_filter_options_selector) { "#{city_filter_selector} > option" }
  let(:event_selector) { 'li.topic' }

  context 'first visit' do
    before do
      create_list(:event, 5)
    end

    it 'shows all events' do
      visit '/'
      expect(all(event_selector).length).to eq 5
      expect(all(city_filter_options_selector).length).to eq(1 + Event.unique_cities.length)
      # save_and_open_page
    end
  end

  context 'filtering' do
    let(:city) { 'Zoolandia' }

    before do
      create(:event)
      @event = create(:event, city: city)
    end

    it 'filters events by city' do
      visit '/'

      expect(all(event_selector).length).to eq(2)
      # select city, from: city_filter_selector.sub('#', '')

      # sleep 1
      # save_and_open_page
      # expect(all(event_selector).length).to eq(1)
    end
  end
end
