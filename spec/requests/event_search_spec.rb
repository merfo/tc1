require 'rails_helper'

RSpec.describe EventSearchController do
  context 'empty search' do
    before { create_list :event, 3 }

    it 'retrieves all events' do
      get '/event_search'
      expect(response).to be_success
      expect(json.size).to eq(3)
      expect(json.first['topic_list']).not_to be_empty
    end
  end

  context 'successful search' do
    let(:city) { 'Maribor' }

    before do
      @event1 = create :event, city: city
      @event2 = create :event, city: city
      @event3 = create :event
    end

    it 'returns filtered events' do
      get '/event_search', city: city
      expect(response).to be_success
      expect(json.size).to eq(2)
      expect(json.first['city']).to eq(city)
      expect(json.second['city']).to eq(city)
    end
  end
end
