require 'rails_helper'

RSpec.describe EventSearch do
  before do
    create_list(:event, 3)
  end

  context 'searching' do
    context 'city search' do
      let!(:city) { 'Booyaka' }
      let!(:event) { create(:event, city: city) }

      it 'should find exactly one event by city' do
        subject.cities << city
        expect(subject.events).to contain_exactly(event)
      end
    end

    context 'topic search' do
      let!(:cool_topic) { create :topic }
      let!(:cool_topic2) { create :topic }

      before do
        @all_events = create_list(:event, 2, topics: [cool_topic]) +
                      create_list(:event, 1, topics: [cool_topic2])
      end

      it 'should find all events having selected topics' do
        subject.topic_ids << [cool_topic.id, cool_topic2.id]
        expect(subject.events).to contain_exactly(*@all_events)
      end
    end

    context 'start date range search' do
      let(:start_date1) { Date.today }
      let(:start_date2) { Date.today + 7 }
      let!(:event1) { create(:event, start_date: start_date1) }
      let!(:event2) { create(:event, start_date: start_date2) }

      it 'should find events by start date inclusively' do
        subject.from_date = start_date1
        subject.to_date = start_date2
        expect(subject.events).to contain_exactly(event1, event2)
      end
    end
  end

  context 'serialization' do
    context 'reify from saved params' do
      let(:from_date) { "2017-01-02" }
      let(:to_date) { "2017-02-02" }
      let(:cities) { ['Monaco', 'Abu dhabi'] }
      let(:topic_ids) { [1,6,8] }
      let(:params) do
          {
            cities: cities,
            topic_ids: topic_ids,
            from_date: from_date,
            to_date: to_date
          }.stringify_keys
      end

      it 'should correctly load a saved search' do
        es = described_class.load_saved(params)
        expect(es.topic_ids).to eq(topic_ids)
        expect(es.cities).to eq(cities)
        expect(es.from_date.strftime('%Y-%m-%d')).to eq(from_date)
        expect(es.to_date.strftime('%Y-%m-%d')).to eq(to_date)
      end
    end
  end
end
