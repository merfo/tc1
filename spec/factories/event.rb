FactoryGirl.define do
  factory :event do
    name { FFaker::Product.product }
    start_date { Date.today - 300 + rand(300) }
    end_date { |event| event.start_date + 1 + rand(5) }
    city { FFaker::Address.city }

    after(:build) do |event|
      event.topic_events << build(:topic_event, event: event)
    end
  end
end
