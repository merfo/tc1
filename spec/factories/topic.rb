FactoryGirl.define do
  factory :topic do
    name { FFaker::Company.name }
  end
end
