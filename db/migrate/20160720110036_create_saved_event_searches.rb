class CreateSavedEventSearches < ActiveRecord::Migration[5.0]
  def change
    create_table :saved_event_searches do |t|
      t.json :params
      t.string :session_id
    end
  end
end
