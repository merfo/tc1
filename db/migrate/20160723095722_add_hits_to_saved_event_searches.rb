class AddHitsToSavedEventSearches < ActiveRecord::Migration[5.0]
  def change
    add_column :saved_event_searches, :hits, :integer, null: false, default: 0
  end
end
