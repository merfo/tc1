class CreateTopicEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :topic_events do |t|
      t.integer :topic_id
      t.integer :event_id

      t.timestamps
    end

    add_foreign_key :topic_events, :topics
    add_foreign_key :topic_events, :events
  end
end
