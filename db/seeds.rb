# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#

require 'ffaker'

topics = 5.times.map { Topic.create!(name: FFaker::Company.name) }
cities = 3.times.map { FFaker::Address.city }

10.times do
  topics_count = rand(2) == 0 ? 1 : 2

  Event.create!(
    name: FFaker::Product.product,
    start_date: Date.today - rand(10),
    end_date: Date.today + rand(15),
    city: cities.sample,
    topics: topics_count.times.map { topics.sample }.uniq
  )
end
