class EventSerializer < ActiveModel::Serializer
  attributes :id, :city, :start_date, :end_date, :name, :topic_list

  def topic_list
    @object.topics.map(&:name).sort.join(', ')
  end
end
