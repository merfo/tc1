module ApplicationCable
  class Connection < ActionCable::Connection::Base
    def session
      cookies.encrypted[Rails.application.config.session_options[:key]]
    end
  end
end
