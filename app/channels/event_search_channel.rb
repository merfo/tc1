require 'session_list'
class EventSearchChannel < ApplicationCable::Channel
  delegate :session, to: :connection

  def subscribed
    stream_from "event_search_#{session['session_id']}"
    SessionList.add(session['session_id'])
  end

  def unsubscribed
    SessionList.remove(session['session_id'])
  end
end
