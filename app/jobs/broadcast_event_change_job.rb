require 'session_list'

class BroadcastEventChangeJob < ActiveJob::Base
  def perform(event)
    return if SessionList.count == 0

    find_saved_searches(SessionList.all).each do |ss|
      if ss.search.hits > ss.hits
        ss.save!
        broadcast_events(ss.session_id, ss.search.events)
      end
    end
  end

  protected
  def broadcast_events(session_id, events)
    ActionCable.server.broadcast(
      broadcast_key(session_id),
      events: events.as_json
    )
  end

  def broadcast_key(session_id)
    'event_search_' + session_id
  end

  def find_saved_searches(session_ids)
    SavedEventSearch.where(session_id: session_ids)
  end
end
