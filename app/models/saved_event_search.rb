class SavedEventSearch < ApplicationRecord
  validates :params, presence: true
  validates :session_id, presence: true

  before_save :count_hits

  def search
    EventSearch.load_saved(params)
  end

  private
  def count_hits
    self.hits = search.events.count
  end
end
