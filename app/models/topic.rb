class Topic < ApplicationRecord
  has_many :topic_events, dependent: :delete_all
  has_many :events, through: :topic_events

  validates :name, presence: true, uniqueness: true
end
