class Event < ApplicationRecord
  has_many :topic_events, dependent: :delete_all
  has_many :topics, through: :topic_events

  validates :name, presence: true, uniqueness: true
  validates :city, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true
  validate :atleast_one_topic

  scope :by_city, -> list { where(city: list) }
  scope :by_topic_id, -> list { joins(:topics).where(topics: { id: list }) }
  scope :by_start_date, -> from, to {
    where("events.start_date >= ? AND events.start_date <= ?", from, to)
  }

  after_commit { BroadcastEventChangeJob.perform_later self }

  class << self
    def unique_cities
      distinct.order('city ASC').pluck('city')
    end
  end

  protected
  def atleast_one_topic
    errors.add :base, "One topic is required" if topic_events.empty?
  end
end
