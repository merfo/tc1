require('babel-polyfill');
require('lodash');

// need to list all components here explicitly
global.EventFilter = require('components/event_filter').default;
global.EventList = require('components/event_list').default;
global.Event = require('components/event').default;
global.EventApp = require('components/event_app').default;
