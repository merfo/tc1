import moment from 'moment';

class Event extends React.Component {
  constructor(props){
    super(props);
    this.state = {};
  }

  prettyDate(date) {
    return moment(date).format('LL');
  }

  render() {
    return (
        <li className="topic list-group-item">
          <span className="badge">{this.props.topic_list}</span>
          <div className="event-name">
            {this.props.city}: {this.props.name}
          </div>
          <span className="event-start-date">
            {this.prettyDate(this.props.start_date)}
          </span>
          <span className="event-end-date">
            {this.prettyDate(this.props.end_date)}
          </span>
        </li>
    );
  }
}

export default Event
