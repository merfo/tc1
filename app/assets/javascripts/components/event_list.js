import _ from 'lodash';
import CSSTransitionGroup from 'react-addons-css-transition-group';
//const CSSTransitionGroup = React.addons.CSSTransitionGroup;

class EventList extends React.Component {
  constructor(props) {
    super(props);
    this.old_ids = [];
  }

  render() {
    let events = this.props.events.map(event => <Event {...event} key={event.id} />);

    return (
        <ul className="list-group">
          <CSSTransitionGroup transitionName="live-event" transitionEnterTimeout={700} transitionLeaveTimeout={350}>
            {events}
         </CSSTransitionGroup>
        </ul>
    );
  }
}

export default EventList
