import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import RangePicker from 'react-daterange-picker';
import CheckBoxList from 'react-checkbox-list';

class EventFilter extends React.Component {
  constructor(props) {
    super(props);

    if (this.props.saved_search) {
      let city = null;
      if (this.props.saved_search.cities.length > 0)
        city = this.props.saved_search.cities[0];

      this.state = {
        city: city,
        topic_ids: this.props.saved_search.topic_ids,
        dateRange: {
          start: moment(this.props.saved_search.from_date),
          end: moment(this.props.saved_search.to_date)
        }
      }
    } else {
      this.state = {city: null, topic_ids: [], dateRange: null};
    }

    // bind event handlers
    this.onCityChange = this.onCityChange.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
    this.onTopicChange = this.onTopicChange.bind(this);
  }

  componentWillUpdate(nextProps, nextState) {
    return true;
  }

  filter() {
    this.props.onChange(this.state);
  }

  onTopicChange(data) {
    this.setState({topic_ids: data}, this.filter);
  }

	onDateChange(range, states) {
		this.setState({dateRange: range}, this.filter);
	}

  onCityChange(event) {
    this.setState({city: event.target.value}, this.filter);
  }

  renderCityFilter() {
    let cities = [<option value="" key="0">All cities</option>];
    this.props.cities.forEach(
        city => cities.push(<option value={city} key={city}>{city}</option>)
    );

    return (
      <select id="event-city-filter" onChange={this.onCityChange} value={this.state.city}>
        {cities}
      </select>
    );
  }

  topicData() {
    let topicData = this.props.topics.map(topic => {
      let topic_id = topic.id.toString();
      return {
        checked: this.state.topic_ids.includes(topic_id),
        value: topic.id.toString(),
        label: topic.name
      }
    });
    return topicData;
  }

  dateRange() {
    if (!this.state.dateRange)
      return null;

    return moment.range(
        this.state.dateRange.start,
        this.state.dateRange.end
    )
  }

  render() {
    return (
        <div>
          <RangePicker onSelect={this.onDateChange}  value={this.dateRange()} />
          <div>
            <input type="text"
              value={this.state.dateRange ? this.state.dateRange.start.format('LL') : ""}
              readOnly={true}
              placeholder="Start date"/>
            <input type="text"
              value={this.state.dateRange ? this.state.dateRange.end.format('LL') : ""}
              readOnly={true}
              placeholder="End date" />
          </div>

          <CheckBoxList defaultData={this.topicData()} onChange={this.onTopicChange} />

          {this.renderCityFilter()}
        </div>
    );
  }
}

export default EventFilter
