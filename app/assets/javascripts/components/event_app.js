import _ from 'lodash';

class EventApp extends React.Component {
  constructor(props) {
    super(props);

    // our initial state is passed in via props
    this.state = {events: this.props.events};

    // bind event handler
    this.filter = this.filter.bind(this);
  }

  componentWillMount() {
    let cable_config = {
      connected() { },
			disconnected() { }
    }

    cable_config.received = data => {
      this.setState({events: data.events});
    }

    if (typeof App != 'undefined') {
      // setup actioncable subscription
      App.event_searches = App.cable.subscriptions.create(
        'EventSearchChannel', cable_config
      );
    }
  }

  filter(search) {
    let data = {}

    if (!_.isEmpty(search.city))
      data.city = search.city;

    if (!_.isEmpty(search.dateRange)) {
      data.from = search.dateRange.start.format('YYYY-MM-DD');
      data.to = search.dateRange.end.format('YYYY-MM-DD');
    }

    if (!_.isEmpty(search.topic_ids)) {
      data.topic_ids = search.topic_ids;
    }

    $.ajax({
      url: "/event_search.json",
      data: data
    }).done(events => this.setState({events: events}));
  }

  render() {
    return (
      <div>
        <EventFilter {...this.props} onChange={this.filter} />
        <EventList events={this.state.events} />
      </div>
    );
  }
}

export default EventApp
