class ApplicationController < ActionController::Base
  def saved_event_search
    @saved_event_search ||= SavedEventSearch.find_by_session_id(session.id)
  end
end
