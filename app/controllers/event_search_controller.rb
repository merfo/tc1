class EventSearchController < ApplicationController
  after_action :save_search!

  def show
    render json: search.events
  end

  private
  def search
    es = EventSearch.new

    if search_params[:from].present?
      es.from_date = _parse_date(search_params[:from])
    end

    if search_params[:to].present?
      es.to_date = _parse_date(params[:to])
    end

    if search_params[:topic_ids].present?
      es.topic_ids = search_params[:topic_ids]
    end

    if search_params[:city].present?
      es.cities = Array(search_params[:city])
    end

    es
  end

  def save_search!
    return unless session.id

    (saved_event_search || SavedEventSearch.new(session_id: session.id)).tap do |ses|
      ses.params = search
      ses.save
    end
  end

  def search_params
    params.permit(:from, :to, :city, topic_ids: [])
  end

  def _parse_date(date)
    Date.parse(date)
  rescue ArgumentError
    nil
  end
end
