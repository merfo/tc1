class MainController < ApplicationController
  def index
    render component: 'EventApp', props: props
  end

  private

  def event_finder
    if saved_event_search
      saved_event_search.search.events
    else
      EventSearch.new.events
    end
  end

  def event_data
    ActiveModelSerializers::SerializableResource.new(event_finder, {})
  end

  def props
    {
      topics: Topic.all,
      events: event_data,
      cities: Event.unique_cities,
      saved_search: saved_event_search.try(:params),
      session_id: session.id
    }
  end
end
