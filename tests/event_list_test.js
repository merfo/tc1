import React from 'react';
import Code from 'code';
import Lab from 'lab';
import sinon from 'sinon';
const lab = exports.lab = Lab.script();

import { shallow, mount, render } from 'enzyme';

const suite = lab.suite;
const test = lab.test;
const expect = Code.expect;

import EventFilter from '../app/assets/javascripts/components/event_filter';

import moment from 'moment';

const defaultProps = {
  cities: ['Dubrovnik', 'New Amsterdam'],
  topics: [ { id: 1, name: 'Cool topic' }, { id: 2, name: 'Uber topic' } ]
}

suite('EventFilter', () => {
  test('show cities option', (done) => {
    const wrapper = shallow( <EventFilter {...defaultProps} /> );
    const select = wrapper.find('select');
    const options = select.find('option');

    expect(options.length).to.equal(3);
    expect(options.get(0).props.value).equal('');
    expect(options.get(1).props.value).to.equal(defaultProps.cities[0]);
    expect(options.get(2).props.value).to.equal(defaultProps.cities[1]);

    done();
  });

  test('show topics checkboxes', (done) => {
    const props = defaultProps;
    const wrapper = render( <EventFilter {...props} /> );

    expect(wrapper.find('input[type="checkbox"]').length).to.equal(props.topics.length);

    done();
  });

  test('saved search setups the state correctly', (done) => {
    const from = moment("2016-01-03");
    const to = moment("2016-01-10");
    const saved_search = {
      cities: ['Alabama'],
      topic_ids: [123],
      from_date: moment(from).format(),
      to_date: moment(to).format()
    }
    const props = Object.assign({}, defaultProps, {saved_search: saved_search});

    const wrapper = mount( <EventFilter {...props} /> );

    expect(wrapper.state().city).to.equal('Alabama');
    expect(wrapper.state().topic_ids).to.equal([123]);
    expect(wrapper.state().dateRange.start.unix()).to.equal(from.unix());
    expect(wrapper.state().dateRange.end.unix()).to.equal(to.unix());

    done();
  });
});
