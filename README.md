# SETUP

- requires a running and accessible postgres
- requires redis-server to be installed somewhere in PATH

```
bundle install
npm install
bundle exec rake db:create db:schema:load db:seed
bundle exec foreman start
```

Open http://localhost:3000

** NOTE: foreman starts BOTH rails & redis-server **

# LIVE SEARCH UPDATING

While having a search open in the browser create a new event in the console that matches your currently selected filter and it will appear as it gets pushed to the client.

```
bundle exec rails c
```
```
Event.create(start_date: Date.today, end_date: Date.today+1, name: 'Newbie', topics: [Topic.first])
```

# TESTING
 
- a touch of React component testing with the help of enzyme library
```
npm test
```

- incomplete integration test (React does not play so nice with capybara it looks like ;)
- some controller, class tests
```
bundle exec rspec
```

# MISSING

TODO: testing the background job, testing the logic in EventSearchController, utility methods from Event, full integration testing (user simulation)
