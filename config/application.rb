require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module Tc1
  class Application < Rails::Application
    config.autoload_paths << Rails.root.join('lib')
    # config.api_only = true
		config.browserify_rails.commandline_options = "-t [ babelify --presets [ es2015 react stage-0 ] --plugins [ syntax-async-functions transform-regenerator ] ]"
		# config.browserify_rails.commandline_options = "-t [ babelify --presets [ es2015 react stage-0 ] --plugins [ syntax-async-functions transform-regenerator ] ] --exclude react-dom --exclude react-dom/server --exclude react-addons-test-utils"
  end
end
