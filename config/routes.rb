Rails.application.routes.draw do
  mount ActionCable.server => '/cable'

  resource :event_search, controller: 'event_search', only: :show

  root to: 'main#index'
end
