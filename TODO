Story
You are building a web service that will be used to publish information about
different events in Slovenia (think meetup.com). These events can have different
pre-defined topics.

1. Every event has a start and end date/time, place and list of discussion
topics.
2. Any user can browse all events or filter them by:
- city
- date and range of start times
- topic of interest
3. Users can save their filters and get back to them in the future (ideally
events will appear filtered according to user's preferences automatically when
they come back to your page)
4. When a new event is added to the system that matches a saved filter, the user
is notified immediately if they are on the page at the time, or via email.

Comments
1. Simplicity and clarity are above all.
2. Feel free to interpret any uncertainties as you see fit - we can discuss the
assumptions you make later.
3. Don't forget about good coding style and best practices.
4. You can use any front-end as this test is focused on back-end (RoR) skills
and we realise not everyone loves front-end :). React would be a plus though.
